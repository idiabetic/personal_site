require './test/test_helper'

class BlogpageTest < CapybaraTestCase
  def test_user_can_see_the_homepage
    visit '/blog/1'

    assert page.has_content?('This is a blog!')
    assert_equal 200, page.status_code
  end
end
