require 'rack'
require 'erb'
require 'tilt'

# ./app/controllers/personal_site.rb
class PersonalSite
  def self.call(env)
    case env['PATH_INFO']
    when '/' then index
    when '/about' then about
    when %r{^/blog/} then blog(env['PATH_INFO'])
    else
      static_exist(env['PATH_INFO'])
    end
  end

  def self.static_exist(path)
    possible_static_files = Dir.entries('public')
    formatted_path = path.delete('/')
    if possible_static_files.include?(formatted_path)
      return render_static(path)
    else
      return error
    end
  end

  def self.index
    render_view('index.html')
  end

  def self.error
    ['404',
     { 'Content-Type' => 'text/html' },
     [File.read('./app/views/error.html')]]
  end

  def self.about
    render_view('about.html')
  end

  def self.blog(path)
    possible_blogs = Dir.entries('app/views')
    selected_blog = path.split('/').last

    blog_filename = "blog#{selected_blog}.html"
    if possible_blogs.include?(blog_filename)
      render_view(blog_filename)
    else
      error
    end
  end

  def self.render_view(page, code = '200')
    template = Tilt.new('./app/views/templates/main.erb')
    [code,
     { 'Content-Type' => 'text/html' },
     [template.render { File.read("./app/views/#{page}") }]]
  end

  def self.render_static(asset)
    [200, { 'Content-Type' => 'text/html' }, [File.read("./public/#{asset}")]]
  end

  def self.css
    render_static('main.css')
  end
end
